#include <PS2X_lib.h>

#define ML_PWM_PIN A1
#define ML_DIR_PIN 3

PS2X ps2x;

int error;

void setup() {
  Serial.begin(57600);

  pinMode(ML_PWM_PIN, OUTPUT);
  pinMode(ML_DIR_PIN, OUTPUT);

  error = ps2x.config_gamepad(4,7,6,8, true, true); // pins: clock, command, attention, data, pressure, rumble

  if(error == 0){
    Serial.println("Found controller, configuration successful");
    //TODO: print gamepad bindings
  }
  else if(error == 1){
    Serial.println("No controller found");
  }
  else if(error == 2){
    Serial.println("Controller found but not accepring commands");
  }
  else if(error == 3){
    Serial.println("Controller does not support pressures mode");
  }
}

void loop() {

  if(error == 1){
    return;
  }
  
    ps2x.read_gamepad(false, 0);

    if (ps2x.Analog(PSS_LY) > 128) {
      analogWrite(ML_PWM_PIN, ps2x.Analog(PSS_LY));
      digitalWrite(ML_DIR_PIN, 0);
    }
    else{
      if (ps2x.Analog(PSS_LY) < 127){
        analogWrite(ML_PWM_PIN, 255-ps2x.Analog(PSS_LY));
        digitalWrite(ML_DIR_PIN, 1);
      }
      else{
        analogWrite(ML_PWM_PIN, 0);
      }
    }
    
    Serial.println(ps2x.Analog(PSS_LY));
    delay(50);
}
